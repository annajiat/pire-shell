---
title: "Why Use a High Performance Computing System?"
teaching: 15
exercises: 5
questions:
- "Why would I be interested in High Performance Computing (HPC)?"
- "What can I expect to learn from this course?"
objectives:
- "Be able to describe what an HPC system is."
- "Identify how an HPC system could benefit you."  
keypoints:
- "High Performance Computing (HPC) typically involves connecting to very large computing systems
  elsewhere in the world."
- "These other systems can be used to do work that would either be impossible or much slower or
  smaller systems."
- "The most common method of interacting with such systems is via a command line interface."
---

Frequently, research problems that use computing can outgrow the desktop or laptop computer where
they started:

* A genomics researcher has been using small datasets of sequence data, but soon will be receiving
  a new type of sequencing data that is 10 times as large. It's already challenging to open the
  small datasets on their computer---analyzing these larger datasets will probably crash it.

* A statistics student wants to cross-validate their model. This involves running the model 1000
  times---but each run takes an hour. Running on their laptop will take over a month!

* An engineer is using a fluid dynamics package that has an option to run in parallel. So far, they
  haven't used this option on their desktop, but in going from 2D to 3D simulations, simulation 
  time has more than tripled and it might be useful to take advantage of that feature.

In all these cases, what is needed is access to more computers than can be used at the same time.

> ## And what do you do?
> 
> Talk to your neighbour, office mate or [rubber duck](https://rubberduckdebugging.com/) about your
> research. How does computing help you do your research? 
> How could more computing help you do more or better research?
{: .challenge}


## Doing Analysis or Running Code

### A standard Laptop for standard tasks

Today, people who develop computer codes or analyse data typically work with laptops.
 
{% include figure.html url="" max-width="20%" file="../fig/laptop-33521_640-pixabay.png"
 alt="A standard laptop" caption="" %}
<!-- include figure.html url="" max-width="20%" file="../fig/200px-laptop-openclipartorg-aoguerrero.svg" -->

Let's dissect what resources programs running on a laptop require:
- the keyboard and/or touchpad is used to tell the the computer what to do (**Input**)
- the internal computing resources **Central Processing Unit** and **Memory** perform calculation
- the display depicts progress and results (**Output**)

Schematically, this can be reduced to the following:

{% include figure.html max-width="60%" file="../fig/Simple_Von_Neumann_Architecture.svg"
alt="Schematic of how a computer works" caption="" %}


### When tasks take too long

When the task to solve become more heavy on computations, the operations are typically out-sourced 
from the local laptop or desktop to elsewhere. Take for example the task to find the directions for
your next business trip. The capabilities of your laptop are typically not enough to calculate 
that route spontaneously. So you use website, which in turn runs on a server that is almost 
exclusively not in the same room as you are.

{% include figure.html url="" max-width="20%" file="../fig/servers-openclipartorg-ericlemerdy.svg" 
alt="A rack half full with servers" caption="" %}

Note here, that a server is mostly a noisy computer mounted into a rack cabinet which in turn 
resides in a data center. The internet made it possible that these data centers do not require to 
be nearby your laptop. What people call **the cloud** is mostly a web-service where you can rent 
such servers by providing your credit card details and by putting together the specs of this
remote resource.

The server itself has no direct display or input methods attached to it. But most importantly, it 
has much more storage, memory and compute capacity than your laptop will ever have. In any case,
you need a local device (laptop, workstation, mobile phone or tablet) to interact with this remote 
machine, which people typically call a "server".

### When one server is not enough

If the computational task or analysis to complete is daunting for a single server, larger 
agglomerations of servers are used. These go by the name of clusters or super computers.

{% include figure.html url="" max-width="20%" 
file="../fig/serverrack-openclipartorg-psteinb-basedon-ericlemerdy.svg" alt="A rack with servers"
caption="" %}

The methodology of providing the input data, communicating options and flags as well as retrieving
the results is quite opposite to using a plain laptop. Using a GUI style interface is
often discarded in favor of using the command line. This imposes a double paradigm shift for 
prospective users:

1. they work with the command line (not a GUI style user interface)
2. they work with a distributed set of computers (called nodes)

> ## I have never used a server, have I?
> 
> Take a minute and think about which of your daily interactions with a computer may require a 
> remote server or even cluster to provide you with results. 
{: .challenge}

In the following episodes, we will learn how to connect to a remote HPC system and interact
with it using an appropriate command-line interface.
Once connected to the remote machine, we are usually dropped to a UNIX shell.
We will spend a substantial amount of time on how to interact with a UNIX shell for two reasons:
(1) shell interaction has been the most common way of working with remote HPC systems, and
(2) shell interaction affords much flexibility and power to efficiently automate repetitive tasks.
UNIX shell is useful not only in the HPC context, but also in daily work of a researcher.
For example, processing of bioinformatic data usually involves many steps,
which forms a *workflow* or a *pipeline*.
The same kind of processing has then to be applied many times (e.g. to different parts of the data).
Shell is a helpful tool to increase researcher's productivity.

{% include links.md %}
