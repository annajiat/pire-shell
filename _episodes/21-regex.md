---
title: "Regular Expression"
teaching: 25
exercises: 20
questions:
- "How do I make a regular expression?"
objectives:
- "Understanding the basic syntax of regular expression."
keypoints:
- "FIXME"
---

The `grep`'s real power doesn't come from its options; it comes from
the fact that patterns can include wildcards. (The technical name for
these is **regular expressions**, which is what the "re" in "grep"
stands for.)
Regular expressions are both complex and powerful.
In this episode, we will learn a few basics of regular expression.

In regular expression syntax, a number of characters have reserved meanings.
They are referred to as "metacharacters".

## Flavors of Regular Expression

There are many different variants of regular expressions.
Regular expressions evolve with time---more and more features added
as time goes by.

* **Basic Regular Expression** ("BRE"):
  This is the original regular expression. There are a limited number of
  metacharacters: `.`, `*`, `[`, `]`, `^`, `$`, and `\`.
  For backward compatibility, `grep` uses basic regular expression by default.

* **Extended Regular Expression** ("ERE"):
  Additional metacharacters are introduced (potentially breaking compatibility
  with BRE):
  `?`, `+`, `{`, `}`, `|`, `(`, `)`.
  These extend the power of regular expression.
  To use ERE using grep, invoke `grep -E` or `egrep`.

* Additionally, Perl and Python provide additional extensions to the regular
  expression syntax to make it even more powerful. For example, these variants allow
  matched strings to be extracted programmatically for other various purposes;
  and there are lookahead and look-back matchers.
  Please see the references below.

We will mostly focus on extended regular expression in this lesson.
Therefore, please use `grep -E` when the extended metacharacters are being used.


## Regular Expression Syntax

### Character matchers

In a regex, the following characters (or sequence of characters) have
special meanings:

* A period (`.`) stand for any single character.

* A pair of square brackets (`[` and `]`) encloses a choice of characters to
  match.
  For example: `[AEIOU]` matches one of the capital vowel;
  `[A-Z]` matches any one capital letter.
  `[A-Za-z0-9]` matches any one letter (any case) or digit.

  - The dash (`-`) character is used to denote a range of characters
    (according to the numerical order of the character representation in computer).
    To use dash as part of the match, put it as the first character within the `[...]`
    group.

  - The caret (`^`) character immediately following the opening bracket denotes
    negation.
    For example: `[^0-9]` matches anything but digits 0 through 9.

* Special matchers also exists to denote frequently used character sets:

  - `[[:alpha:]]` --- regular letters (e.g. A-Z and a-ze)
  - `[[:lower:]]` --- lowercase letters
  - `[[:upper:]]` --- uppercase letters
  - `[[:digit:]]` --- digits 0-9
  - `[[:alnum:]]` --- regular A-Z letters (any case) and digits 0-9
  - `[[:punct:]]` --- punctuation characters such as `.`, `,`, `!`, `?`, `=`, and so on.

  A full reference for these character sets can be found on
  [GNU grep document](https://www.gnu.org/software/grep/manual/grep.html#Character-Classes-and-Bracket-Expressions).

These matchers match *exactly* a single character.

> ## The Babel Effect
>
> Today, with the advent of UNICODE, computer finally supports multiple language
> and letter systems in a seamless manner.
> However, the definition of letters and numbers differs based on what human
> language is being used.
> In matching process, many regex libraries will respect the native language,
> which may affect what characters match a given specifier.
> This is an advanced topic; interested reader should do some more research.
> For `grep`, for example, one can start with the
> [Environment Variables](https://www.gnu.org/software/grep/manual/grep.html#Environment-Variables)
> section of GNU grep documentation.
>
{: .callout}


> ## Exercises
>
> Build a regex to match a 3-letter word that has a vowel in the middle (second
> character). All the other two characters can be any letter (a-z, irrespective
> of letter case).
> For example, this regex should match: `pop`, `hot`, `bad`, `sit`, `see`, but not
> `awe`, `ivy`.
> Test your regex against LittleWomen.txt.
{: .challenge}

> ## Exercises
>
> Building on the previous regex, create a pipeline that will calculate the
> frequency of the 3-letter words (that has a vowel in the second character),
> show it in descending order.
>
> Hint: use `sort` and `uniq`, as many times as needed.
> Test your pipeline against LittleWomen.txt.
>
>> ## Solution
>>
>> There are more than one possible solution.
>> ~~~
>> $ grep -ow '[A-Za-z][AEIOUaeiou][A-Za-z]' LittleWomen.txt | sort | uniq -c | sort -r -n 
>> ~~~
>> {: .bash}
>> ~~~
>>    3192 her
>>    2168 for
>>    2121 you
>>    2028 was
>>    1261 but
>>    1097 his
>>    1062 had
>>     873 not
>>     778 him
>>     683 Meg
>>         ...
>>       1 cal
>>       1 cab
>>       1 BUT
>>       1 bun
>>       1 bug
>>       1 Boy
>>       1 bog
>>       1 Ain
>>       1 Aim
>>       1 Aid
>> ~~~
>> {: .output}
>{: .solution}
{: .challenge}

### Count Specifiers

A few metacharacters specify the count of a character matcher just preceding
each of them, as tabulated below. Here, `N` and `M` refer to integer numbers
(where `M >= N`),
and `B` or `E` on the third column refers to basic or extended regular expression
syntax:

|-------------------------------|---------------------------------------------------|---------|
| Metacharacter                 | Meaning                                           | Variant |
|-------------------------------|---------------------------------------------------|---------|
| *                             | Zero or more matches                              | B, E    |
| +                             | One or more matches                               | E       |
| ?                             | Zero or one match                                 | E       |
| {N}                           | Matches exactly `N` times                         | E       |
| {N,}                          | Matches `N` times or more                         | E       |
| {N,M}                         | Matches between `N` and `M` times, inclusive      | E       |
| {,M}                          | Matches at most `M` times                         | E       |
|-------------------------------|---------------------------------------------------|---------|

> ## Quote Your Regular Expressions!
>
> Due to the possible collision with shell special characters, please quote your
> regex with single quote. For example, to extract four-digit numbers from LittleWomen:
>
> ~~~
> $ grep -E -w '[0-9]{4}' LittleWomen.txt
> ~~~
{: .callout}




## References

* [GNU grep documentation](https://www.gnu.org/software/grep/manual/grep.html)

* Extended & basic regular expressions (as understood by GNU grep):

  - man page: <https://linux.die.net/man/1/grep>

  - longer documentation:
    <https://www.gnu.org/software/grep/manual/grep.html#Regular-Expressions>

* Perl regular expressions:
  <https://perldoc.perl.org/perlre.html>

* Python regular expressions:
  <https://docs.python.org/3/library/re.html>