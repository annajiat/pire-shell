---
title: Shell Variables
teaching: 10
exercises: 0
questions:
- "How to create shell variables?"
- "How to change shell variables?"
objectives:
- "Understanding shell variables"
keypoints:
- "Shell variables provide a way to store and modify parameters."
- "Shell variables can affect the environment and behavior of programs running under the shell."
---

The shell is just a program, and like other programs, it has variables.
Those variables control its execution,
so by changing their values
you can change how the shell and other programs behave.

Let's start by running the command `set` and looking at some of the variables in a typical shell session:

~~~
$ set
~~~
{: .bash}

~~~
COMPUTERNAME=TURING
HOME=/home/vlad
HOMEDRIVE=C:
HOSTNAME=TURING
HOSTTYPE=i686
NUMBER_OF_PROCESSORS=4
OS=Windows_NT
PATH=/Users/vlad/bin:/usr/local/git/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin
PWD=/Users/vlad
UID=1000
USERNAME=vlad
...
~~~
{: .output}

This is an example taken from a Windows computer.
As you can see, there are quite a few---in fact, four or five times more than what's shown here.
And yes,
using `set` to *show* things might seem a little strange,
even for Unix,
but if you don't give it any arguments,
it might as well show you things you *could* set.

Every variable has a name.
By convention, variables that are always present are given upper-case names, starting
with an uppercase letter.
Variable names can contain letters (both upper and lower case), underscores, and
numbers, but it needs to start with a letter or an underscore.
All shell variables' values are strings, even those (like `UID`) that look like numbers.
It's up to programs to convert these strings to other types when necessary.
For example, if a program wanted to find out how many processors the computer had,
it would need to convert the value of the `NUMBER_OF_PROCESSORS` variable from a string
to an integer.

> ## Exploration: Shell variables on Turing
>
> Type `set` on Turing and inspect the variables that are defined.
> Can you recognize some of these?
{: .challenge}

Some variables of interest are:

- `HOME`
- `HOSTNAME`
- `GROUP`
- `PATH`
- `PS1`
- `SHELL`
- `USER`

These variables are initialized by the shell when it is started.
As you can see, there is a lot of information made available by the shell.


## Showing the Value of a Variable

Let's show the value of the variable `HOME`:

~~~
$ echo HOME
~~~
{: .bash}

~~~
HOME
~~~
{: .output}

That just prints "HOME", which isn't what we wanted
(though it is what we actually asked for).
Let's try this instead:

~~~
$ echo $HOME
~~~
{: .bash}

~~~
/Users/vlad
~~~
{: .output}

The dollar sign tells the shell that we want the *value* of the variable
rather than its name.
This works just like wildcards:
the shell does the replacement *before* running the program we've asked for.
Thanks to this expansion, what we actually run is `echo /Users/vlad`,
which displays the right thing.

## Creating and Changing Variables

Creating a variable is easy&mdash;we just assign a value to a name using "=":

~~~
$ SECRET_IDENTITY=Dracula
$ echo $SECRET_IDENTITY
~~~
{: .bash}

~~~
Dracula
~~~
{: .output}

To change the value, just assign a new one:

~~~
$ SECRET_IDENTITY=Camilla
$ echo $SECRET_IDENTITY
~~~
{: .bash}

~~~
Camilla
~~~
{: .output}

> ## Trouble with Whitespaces
>
> Bash statements such as `SECRET_IDENTITY=Dracula` have the usual construct of
> variable assignment, and if you program in other programming languages, you may be
> tempted to add spaces around the `=` sign:
>
> ~~~
> $ SECRET_IDENTITY = Dracula
> ~~~
> {: .bash}
>
> What will happen in this case?
> Did the variable `SECRET_VARIABLE` get set?
>
>> ## Solution
>>
>> The statement above will issue in an  error:
>> ~~~
>> bash: SECRET_IDENTITY: command not found
>> ~~~
>> {: .output}
>> This example shows one of the quirks of Unix shell.
>> No whitespace is allowed in the variable assignment.
>> If the value contain whitespaces, then the value must be quoted.
>{: .solution}
{: .challenge}



## The `PATH` Variable

Some variables (like `PATH`) store lists of values.
In this case, the convention is to use a colon ':' as a separator.
If a program wants the individual elements of such a list,
it's the program's responsibility to split the variable's string value into pieces.

Let's have a closer look at that `PATH` variable.
Its value defines the shell's [search path]({{ page.root }}/reference/{{ site.index }}#search-path),
i.e., the list of directories that the shell looks in for runnable programs
when you type in a program name without specifying what directory it is in.

For example,
when we type a command like `analyze`,
the shell needs to decide whether to run `./analyze` or `/bin/analyze`.
The rule it uses is simple:
the shell checks each directory in the `PATH` variable in turn,
looking for a program with the requested name in that directory.
As soon as it finds a match, it stops searching and runs the program.

To show how this works, Vlad prints out the value of `$PATH` on his shell:

~~~
$ echo $PATH
~~~
{: .bash}
~~~
/Users/vlad/bin:/usr/local/git/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin
~~~
{: .output}

Here are the components of `PATH` listed one per line:

~~~
/Users/vlad/bin
/usr/local/git/bin
/usr/bin
/bin
/usr/sbin
/sbin
/usr/local/bin
~~~
{: .output}

On Vlad's computer,
there are actually three programs called `analyze`
in three different directories:
`/bin/analyze`,
`/usr/local/bin/analyze`,
and `/Users/vlad/analyze`.
Since the shell searches the directories in the order they're listed in `PATH`,
it finds `/bin/analyze` first and runs that.
Notice that it will *never* find the program `/Users/vlad/analyze`
unless we type in the full path to the program,
since the directory `/Users/vlad` isn't in `PATH`.

> ## Finding Out Where an Executable is Located
>
> A real Unix environment often has a long list of directories in `PATH`.
> Which one contains a particular executable?
> This can be daunting if one has to look for this manually.
> Fortunately, there is an utility called `which` which looks for the first
> found executable in the `PATH` list.
>
> For example:
> ~~~
> $ which ls
> ~~~
> {: .bash}
> ~~~
> /bin/ls
> ~~~
> {: .output}
{: .callout}

> ## Explore Turing
>
> Find where the following programs are located on Turing:
>
> - `mv`
> - `bash`
> - `sbatch`
> - `python`
> - `R`
> - `fastqc`
>
> Why can't some of these programs be found?
{: .challenge}

> ## Libraries, Python, R ... Oh My!
>
> The concept in `PATH` variable is frequently encountered in Unix computing.
> For example:
>
> - `LD_LIBRARY_PATH` is used to look for necessary libraries to run binary
>   executable programs
>
> - `PYTHONPATH` is used to look for necessary Python modules when running a
>   Python script.
>
> - `R_LIBS` and `R_LIBS_USER` are used by R to look for packages needed by
>   an R script.
>
> In all these cases, the way a library or file is searched is the same as
> the case of `PATH`.
{: .callout}


## Initializing Variables

If we want to set some variables automatically every time we run a shell,
we can put commands to do this in a file called `.bashrc` in our home directory.
(The '.' character at the front prevents `ls` from listing this file
unless we specifically ask it to using `-a`:
we normally don't want to worry about it.
The "rc" at the end is an abbreviation for "run control",
which meant something really important decades ago,
and is now just a convention everyone follows without understanding why.)

For example,
here are two lines in `/Users/vlad/.bashrc`:

~~~
export SECRET_IDENTITY=Dracula
export TEMP_DIR=/tmp
export BACKUP_DIR=$TEMP_DIR/backup
~~~
{: .output}

These three lines create the variables `SECRET_IDENTITY`,
`TEMP_DIR`,
and `BACKUP_DIR`,
and export them so that any programs the shell runs can see them as well.
Notice that `BACKUP_DIR`'s definition relies on the value of `TEMP_DIR`,
so that if we change where we put temporary files,
our backups will be relocated automatically.

> ## To `export` or Not to `export`?
>
> Bash recognizes two kinds of variables: internal (private) variables and
> environmental (public) variables.
> In bash itself, there is no difference between the two---the value of these
> variables are available via the usual `$VARNAME` syntax.
>
> Variables that are initialized with just `VARNAME=value` are private variables.
> These variables can be made public by using the `export` statement, either
>
> ~~~
> $ VARNAME=value
> $ export VARNAME
> ~~~
> {: .bash}
>
> A variable can also be set and declared as an environmental variable at once:
>
> ~~~
> $ export VARNAME=value
> ~~~
> {: .bash}
>
> The names and values of environmental variables are passed on to programs that are
> run by this shell.
> Let's consider an example where we invoke a Python script named `analyze.py`.
> The `python` interpreter will be able to see the `PATH` and `PYTHONPATH` values
> from within because it is passed on by the shell to the Python interpreter
> when it is started.
> Environmental variables is a way to pass on values to the programs called
> by the shell.
> In the case of `PYTHONPATH`, it indeed affects the way Python looks for the
> necessary modules.
> For example, it can be used to direct Python to load a newer Numpy library
> (version 2.0 instead of the older 1.3 version available otherwise).
>
> Many of the standard Unix variables such as `USER`, `HOSTNAME`, `PATH` are
> exported, therefore available to programs invoked by the shell.
>
{: .callout}

{% include links.md %}
