---
title: "Credits for materials used"
---

## Lesson credits

* The Unix Shell ("shell-novice") from Software Carpentry

  <https://swcarpentry.github.io/shell-novice/>

* Introduction to High-Performance Computing ("hpc-intro") from HPC Carpentry

  <https://hpc-carpentry.github.io/hpc-intro/>

* Introduction to Using the Shell in a High-Performance Computing Context
  ("hpc-shell") from HPC Carpentry

  <https://hpc-carpentry.github.io/hpc-shell/>

## Image credits

* Laptop image with blue screen monitor, from Pixabay user `Clker-Free-Vector-Images`

  <https://pixabay.com/vectors/laptop-black-blue-screen-monitor-33521/>


{% include links.md %}
